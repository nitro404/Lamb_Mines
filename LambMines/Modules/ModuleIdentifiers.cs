namespace LambMines
{
    /** @enum   MODULE_IDENTIFIER
    *   @brief  numerical identifiers for the module types
    */
    public enum MODULE_IDENTIFIER
    {
        MID_THIS            = 0,    //Continue using the current module
        MID_SPLASH_MODULE,
        MID_MENU_MODULE,
        MID_GAME_MODULE,
        MID_WIN_MODULE,
		MID_LOSE_MODULE
    };
}