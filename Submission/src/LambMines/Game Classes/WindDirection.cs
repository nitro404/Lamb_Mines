﻿
namespace Scallywags
{
    /** @enum   WindDirection
     *  @brief  the possible directions of the wind
     */
    public enum WindDirection
    { 
        WD_EAST         = 0,
        WD_NORTHEAST    = 1,
        WD_NORTH        = 2,
        WD_NORTHWEST    = 3,
        WD_WEST         = 4,
        WD_SOUTHWEST    = 5,
        WD_SOUTH        = 6,
        WD_SOUTHEAST    = 7       
    };
}